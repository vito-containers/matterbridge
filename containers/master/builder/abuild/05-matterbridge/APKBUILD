# Contributor: Vitoyucepi <vitoyucepi@cock.li>
# Maintainer: Vitoyucepi <vitoyucepi@cock.li>

pkgname=matterbridge
pkgver=1.26.1_git20240827
pkgrel=99
_commit=c4157a4d5b49fce79c80a30730dc7c404bacd663
pkgdesc='Multi-protocols (IRC/XMPP/Mattermost/Slack/Matrix/etc) bridge'
url='https://github.com/42wim/matterbridge'
arch='all'
license='Apache-2.0'
install="$pkgname.pre-install"

# renovate: datasource=repology depName=alpine_3_21/mailcap versioning=loose
depends="$depends mailcap=2.1.54-r0"
# renovate: datasource=repology depName=alpine_3_21/ca-certificates versioning=loose
depends="$depends ca-certificates=20241121-r1"

# renovate: datasource=repology depName=alpine_3_21/go versioning=loose
makedepends="$makedepends go=1.23.6-r0"

source="$pkgname-$pkgver.tar.gz::https://github.com/42wim/matterbridge/archive/$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"

pkgusers='matterbridge'
pkggroups='matterbridge'

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-v \
		-o matterbridge \
		-tags whatsappmulti
}

check() {
	go test -short ./...
}

package() {
	install -Dm755 matterbridge "$pkgdir"/usr/bin/matterbridge
}

sha512sums="
58945dacbd5cab44c3b930a12ddbf2824064a4c5d6855ae10ea100f48840c46c41c8750d66eb2684d3c14f3dbe3f6dfed83e48fac87570cce08712f842c76a29  matterbridge-1.26.1_git20240827.tar.gz
"
